package dvl.srg.main;


import dvl.srg.discovery.DiscoveryClient;
import dvl.srg.model.Employee;
import dvl.srg.model.Metadates;
import dvl.srg.transport.TransportClient;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("[INFO] -----------------------------------------\n" +
                "[INFO] Client is running...");

        try {
            Metadates nodeDates = new DiscoveryClient(
                    new InetSocketAddress("127.0.0.1", 30000))
                    .retrieveNodeDates();
            System.out.println("[INFO] -----------------------------------------\n" +
                    "[INFO] Discovered maven server: " + nodeDates);


            if (nodeDates != null) {
                ArrayList<Employee> employeeLis =  new TransportClient()
                                                    .getEmployeesFrom(nodeDates, "");
                showFiltered(employeeLis);

                System.out.println("[SALARY AVERAGE] --------------------------------------");
                System.out.println("Average result: " + employeeLis
                                                            .stream()
                                                            .mapToDouble((employee) -> employee.getSalary())
                                                            .average()
                                                    );

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void showFiltered(ArrayList<Employee> list) {
        System.out.println("[Result] -----------------------------------------\n" +
                        "Discovered employees: " +
                        list.stream()
                                .filter(e -> e.getSalary() > 6000.0)
                                .sorted(Comparator.comparing(Employee::getLastName))
                                .collect(Collectors.groupingBy(Employee::getDepartment))
                                .toString()
        );
    }
}
