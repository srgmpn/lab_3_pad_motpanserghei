package dvl.srg.main;

import dvl.srg.bfs.BFS;
import dvl.srg.discovery.DiscoveryListener;
import dvl.srg.model.Metadates;
import dvl.srg.transport.TransportListener;
import dvl.srg.utils.ReadAdjListHelper;
import dvl.srg.utils.TimerHelper;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int serverPort = 20000;
        int nodeNr = 0;
        int nrNodes = 0;

        if (args.length != 2) {
            System.out.println("[ERROR] ----- set args : <nr server> <port>");
        }

        nodeNr = Integer.parseInt(args[0]);
        serverPort = Integer.parseInt(args[1]);


        //read server relations configs
        Map<String, ArrayList<String>> mapListAdj = ReadAdjListHelper.read();
        nrNodes = mapListAdj.get(""+nodeNr).size();

        //get bfs list adiacent nodes
        String jsonBFSList  = new BFS()
                .getBFSNodes(mapListAdj, "" + nodeNr);

        InetSocketAddress serverLocation = new InetSocketAddress("127.0.0.1", serverPort);
        Metadates serverMetadates = new Metadates(serverLocation, nodeNr, nrNodes);

        //System.out.println(serverMetadates);
        System.out.println("[INFO] -----------------------------------------\n" +
                "[INFO] Server " + nodeNr + " is running... on " + serverPort);

        new DiscoveryListener(serverMetadates)
                .start();

        TransportListener transportListener = new TransportListener(serverMetadates, jsonBFSList);
        transportListener.start();

        TimerHelper.sleep(30000);
        transportListener.setStopped(true);
    }
}
