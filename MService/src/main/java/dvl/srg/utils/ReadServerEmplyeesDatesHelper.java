package dvl.srg.utils;

import dvl.srg.model.Employee;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by administrator on 10/12/15.
 */
public class ReadServerEmplyeesDatesHelper {

    public static ArrayList<Employee> getEmployeesFromFile(int nodeNr) {
        ArrayList<Employee> employees = null;
        try{
            File inputFile = new File("./config/node" + nodeNr + ".xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            XPath xPath =  XPathFactory.newInstance().newXPath();

            String expression = "/employees/employee";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);

            employees = new ArrayList<>();

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    Employee employee = new Employee();
                    employee.setFirstName(eElement.getElementsByTagName("firstName")
                                .item(0)
                                .getTextContent());
                    employee.setLastName(eElement.getElementsByTagName("lastName")
                            .item(0)
                            .getTextContent());
                    employee.setDepartment(eElement.getElementsByTagName("department")
                            .item(0)
                            .getTextContent());
                    employee.setSalary(Double.valueOf(eElement.getElementsByTagName("salary")
                            .item(0)
                            .getTextContent()));
                    employees.add(employee);
                }
            }
        }catch (SAXException | XPathExpressionException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
