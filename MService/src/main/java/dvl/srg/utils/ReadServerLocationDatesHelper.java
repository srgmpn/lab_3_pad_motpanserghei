package dvl.srg.utils;

import dvl.srg.model.Metadates;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by administrator on 10/12/15.
 */
public class ReadServerLocationDatesHelper {

    public static Metadates getModeMetadates(String node) {
        Metadates metadates = new Metadates();

        try {

            File inputFile = new File("./config/nodesConfig.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            XPath xPath = XPathFactory.newInstance().newXPath();


            String expression = "/nodes/node[@id='" + node + "']";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);


            Node nNode = nodeList.item(0);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                String id = eElement.getAttribute("id");

                String host = eElement.getElementsByTagName("locationHost")
                                .item(0)
                                .getTextContent();
                int port = Integer.parseInt(eElement.getElementsByTagName("locationPort")
                        .item(0)
                        .getTextContent());
                metadates.setLocation(new InetSocketAddress(host, port));
                metadates.setNrNodes(Integer.parseInt(eElement.getElementsByTagName("nrNodes")
                        .item(0)
                        .getTextContent()));
                metadates.setServerNr(Integer.parseInt(node));
            }


        } catch (ParserConfigurationException | SAXException | XPathExpressionException | IOException e) {
            e.printStackTrace();
        }

        return metadates;
    }
}
