package dvl.srg.utils;

/**
 * Created by administrator on 10/12/15.
 */
public class Constants {

    public static final String PROTOCOL_GROUP_ADDRESS = "224.10.10.5";
    public static final int PROTOCOL_GROUP_PORT = 10000;
    public static final int PROTOCOL_GROUP_TIMEOUT = 10; // seconds

}
