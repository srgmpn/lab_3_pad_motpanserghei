package dvl.srg.model;

import java.io.Serializable;
import java.net.InetSocketAddress;

/**
 * Created by administrator on 10/12/15.
 */
public class Metadates implements Serializable {

    private static final long serialVersionUID = -8676816449888969864L;


    private InetSocketAddress location;
    private int serverNr;
    private int nrNodes;

    public Metadates() {

    }

    public Metadates(InetSocketAddress location) {
        this.location = location;
    }

    public Metadates(InetSocketAddress location, int serverNr, int nrNodes) {
        this.location = location;
        this.serverNr = serverNr;
        this.nrNodes = nrNodes;
    }

    public InetSocketAddress getLocation() {
        return location;
    }

    public void setLocation(InetSocketAddress location) {
        this.location = location;
    }

    public int getServerNr() {
        return serverNr;
    }

    public void setServerNr(int serverNr) {
        this.serverNr = serverNr;
    }

    public int getNrNodes() {
        return nrNodes;
    }

    public void setNrNodes(int nrNodes) {
        this.nrNodes = nrNodes;
    }

    @Override
    public String toString() {
        return "Metadates{" +
                "locationHost=" + location.getHostString() +
                ", locationPort=" + location.getPort() +
                ", serverNr=" + serverNr +
                ", nrNodes=" + nrNodes +
                '}';
    }
}
