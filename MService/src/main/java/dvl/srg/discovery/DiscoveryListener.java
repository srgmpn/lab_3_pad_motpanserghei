package dvl.srg.discovery;

import dvl.srg.model.Metadates;
import dvl.srg.utils.Constants;

import java.io.IOException;
import java.net.*;

import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;

/**
 * Created by administrator on 10/12/15.
 */
public class DiscoveryListener extends Thread {
    private Metadates serverMetadates;

    public DiscoveryListener(Metadates serverMetadates) {
        this.serverMetadates = serverMetadates;
    }

    @Override
    public void run() {
        sendDataServerLocation(
                receiveClientRequest(),
                serverMetadates);
    }

    /**
     * Receives request through UDP multicast and returns client location where
     * "discovery" listener must send information (address and port) about data
     * transport server.
     */
    private Metadates receiveClientRequest() {
        Metadates clientLocation = null;
        try {
            MulticastSocket s = new MulticastSocket(Constants.PROTOCOL_GROUP_PORT);
            s.joinGroup(InetAddress.getByName(Constants.PROTOCOL_GROUP_ADDRESS));

            byte buf[] = new byte[2048];
            DatagramPacket pingPacket = new DatagramPacket(buf, buf.length);
            s.receive(pingPacket);

            clientLocation = deserialize(pingPacket.getData());

            System.out.println("[INFO] -----------------------------------------\n" +
                    "[INFO] Received metadates request...");

            s.leaveGroup(InetAddress.getByName(Constants.PROTOCOL_GROUP_ADDRESS));
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clientLocation;
    }

    /**
     * Sends data server location using UDP unicast
     *
     * @param clientLocation where information is send
     * @param serverLocation where client app can request data collection
     */
    private void sendDataServerLocation(Metadates clientLocation, Metadates serverLocation) {
        try {
            byte[] sendDataServerAddress = serialize(serverLocation);
            DatagramSocket clientSocket = new DatagramSocket();

            DatagramPacket pongPacket = new DatagramPacket(sendDataServerAddress,
                    sendDataServerAddress.length,
                    clientLocation.getLocation());
            clientSocket.send(pongPacket);

            clientSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
