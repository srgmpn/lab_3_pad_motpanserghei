package dvl.srg.discovery;

import dvl.srg.model.Metadates;
import dvl.srg.utils.Constants;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;

/**
 * Created by administrator on 10/12/15.
 */
public class DiscoveryClient {

    private InetSocketAddress clientAddress;

    /**
     * @param clientAddress client location where discovery servers send data locations
     */
    public DiscoveryClient(InetSocketAddress clientAddress) {
        this.clientAddress = clientAddress;
    }

    public Metadates retrieveNodeDates() throws IOException {
        ArrayList<Metadates> nodeDates = null;

        sendNodeDatesRequest();
        nodeDates = receiveNodeDates();


        if (nodeDates.size() > 0) {
            return getMavenNode(nodeDates);
        } else {
            return null;
        }
    }

    /**
     * Receives UDP unicast datagrams which include
     * server nodeDates of distributed data collections
     *
     * @throws IOException
     */
    private ArrayList<Metadates> receiveNodeDates() throws IOException {

        ArrayList<Metadates> nodeDates = new ArrayList<>();
        DatagramSocket datagramServer = new DatagramSocket(clientAddress);
        byte dataFromServer[] = new byte[2048];
        boolean isTimeExpired = false;
        datagramServer.setSoTimeout((int) SECONDS.toMillis(Constants.PROTOCOL_GROUP_TIMEOUT));

        System.out.println("[INFO] -----------------------------------------\n" +
                "[INFO] Discovering... information nodes.");
        while (!isTimeExpired) {
            DatagramPacket pongPacket = new DatagramPacket(dataFromServer, dataFromServer.length);
            try {

                datagramServer.receive(pongPacket);
            } catch (SocketTimeoutException e) {
                System.out.println("[WARNING] -----------------------------------------\n" +
                        "[WARNING] Waiting time expired...");
                isTimeExpired = true;
                continue;
            }
            nodeDates.add((Metadates) deserialize(pongPacket.getData()));
            System.out.println("[INFO] " +
                    "Receiving reply from: (" +
                    pongPacket.getPort() + ", " +
                    pongPacket.getAddress().getHostAddress() + ")");
        }
        datagramServer.close();
        return nodeDates;
    }

    /**
     * Sends UDP multicast request to node group of distributed system.
     * Request includes client address used by discovery listener.
     *
     * @throws IOException
     */

    private void sendNodeDatesRequest() throws IOException {
        MulticastSocket s = new MulticastSocket();
        byte sendData[] = serialize(new Metadates(clientAddress));
        DatagramPacket pingPacket = new DatagramPacket(sendData, sendData.length,
                InetAddress.getByName(Constants.PROTOCOL_GROUP_ADDRESS),
                Constants.PROTOCOL_GROUP_PORT);
        s.send(pingPacket);
        s.close();
    }

    private Metadates getMavenNode(ArrayList<Metadates> nodesMetadates) {

        final List<Integer> values = new ArrayList<>(nodesMetadates.size());
        nodesMetadates.forEach((node) -> values.add(node.getNrNodes()));
        int max = Collections.max(values);

        for (Metadates mavenNode: nodesMetadates) {
            if (max == mavenNode.getNrNodes()) {
                return mavenNode;
            }
        }
        return nodesMetadates.get(0);
    }
}
