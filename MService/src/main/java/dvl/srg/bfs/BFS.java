package dvl.srg.bfs;

import com.google.gson.Gson;

import java.util.*;

/**
 * Created by administrator on 10/12/15.
 */
public class BFS {

    private Queue<String> queue;

    public BFS() {
        queue = new LinkedList<>();
    }

    public  String getBFSNodes(Map<String, ArrayList<String>> adjacencyList, String source) {

        int number_of_nodes = adjacencyList.size();
        Map<String, Object> bfsNodeList = new HashMap<>();

        int[] visited = new int[number_of_nodes + 1];
        String element = source;

        visited[Integer.parseInt(source)] = 1;
        queue.add("" + source);

        while (!queue.isEmpty())
        {
            element = queue.remove();

            String nodes = "";
            for (String s: adjacencyList.get(element)) {

                if (visited[Integer.parseInt(s)] == 0) {
                    queue.add(s);
                    nodes += s + " ";
                    visited[Integer.parseInt(s)] = 1;
                }
            }
            bfsNodeList.put(element, nodes);
        }

        return new Gson().toJson(bfsNodeList);//jsonObject.toJSONString();
    }
}
