package dvl.srg.transport;

import dvl.srg.model.Employee;
import dvl.srg.model.Metadates;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import static org.apache.commons.lang3.SerializationUtils.deserialize;

/**
 * Created by administrator on 10/12/15.
 */
public class TransportClient implements Callable<ArrayList<Employee>>{

    private Metadates metadates;
    private String jsonBFSList;

    public TransportClient() {
        jsonBFSList = "";
    }

    public TransportClient(Metadates metadates, String jsonBFSList) {
        this.metadates = metadates;
        this.jsonBFSList = jsonBFSList;
    }

    @Override
    public ArrayList<Employee> call() throws Exception {
        return getEmployeesFrom(metadates, jsonBFSList);
    }

    public ArrayList<Employee> getEmployeesFrom(Metadates metadates, String jsonBFSList) throws IOException {
        Socket socket = new Socket();
        socket.connect(metadates.getLocation());
        writeOutputStream(socket, jsonBFSList);
        return readInputData(socket);
    }

    private void writeOutputStream(Socket socket, String jsonBFSList) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        outputStream.writeObject(jsonBFSList);
        outputStream.flush();
    }

    private ArrayList<Employee> readInputData(Socket socket) throws IOException {
        Employee[] employees = (Employee[]) deserialize(socket.getInputStream());
        socket.close();
        return new ArrayList<Employee>(Arrays.asList(employees));
    }
}
