package dvl.srg.transport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.model.Employee;
import dvl.srg.model.Metadates;
import dvl.srg.utils.ReadServerEmplyeesDatesHelper;
import dvl.srg.utils.ReadServerLocationDatesHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.apache.commons.lang3.SerializationUtils.serialize;

/**
 * Created by administrator on 10/12/15.
 */
public class TransportListener extends Thread {

    private Metadates serverMetadates;
    private boolean isStopped;
    private boolean isAccepted;
    private ServerSocket serverSocket;
    private String jsonBFSList;
    private String jsonInputStream;

    public TransportListener(Metadates serverMetadates, String jsonBFSList) {
        this.jsonBFSList = jsonBFSList;
        this.serverMetadates = serverMetadates;
        isStopped = false;
    }

    @Override
    public void run() {

        try {
            serverSocket = new ServerSocket(serverMetadates.getLocation().getPort());
            while (!isStopped) {
                Socket socket = serverSocket.accept();  // Blocking call!
                // You can use non-blocking approach
                isAccepted = true;
                readData(socket);
                writeData(socket);
                isAccepted = false;
            }
        } catch (SocketException e) {
            System.out.println("[WARNING] ----------------------------------------- \n" +
                    "[WARNING] Waiting time expired... Socket is closed.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readData(Socket socket) throws IOException, ClassNotFoundException {
        jsonInputStream = (String) new ObjectInputStream(socket
                .getInputStream())
                .readObject();
    }

    private void writeData(Socket socket) throws IOException {
        ArrayList<Employee> employees = getEmployees();
        Employee[] s = new Employee[employees.size()];
        serialize((Employee[]) employees.toArray(s), socket.getOutputStream());
        socket.getOutputStream().flush();
        socket.close();
    }

    public void setStopped(boolean isStopped) {
        this.isStopped = isStopped;
        if (!isAccepted) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Employee> getEmployees() throws IOException {

        if (jsonInputStream.isEmpty()) {
            jsonInputStream = jsonBFSList;
        }

        ArrayList<Employee> employees = getNodeEmpoyeesDates();

        Map<String, Object> listBFS = getBFSJsonNodesMapObject();

        if (listBFS.get(serverMetadates.getServerNr() + "").toString().isEmpty()) {
            return employees;
        }


        return getEmployeesFromFuture(
                            getArrayListFutureListEmployees(
                                    listBFS)
                                , employees);
    }

    private ArrayList<Employee> getNodeEmpoyeesDates() {
        return  ReadServerEmplyeesDatesHelper
                .getEmployeesFromFile(serverMetadates
                        .getServerNr());
    }

    private  Map<String, Object>  getBFSJsonNodesMapObject() {
        return new Gson().fromJson(jsonInputStream,
                new TypeToken<Map<String, Object>>() {
                }.getType());
    }

    private Future<ArrayList<Employee>> [] getArrayListFutureListEmployees(Map<String, Object> listBFS) {
        StringTokenizer stringTokenizer = new StringTokenizer(listBFS.get(serverMetadates.getServerNr() + "").toString(), " ");

        ExecutorService service = Executors.newFixedThreadPool(stringTokenizer.countTokens());
        Future<ArrayList<Employee>> [] arrayListFuture = new Future[stringTokenizer.countTokens()];
        int i = 0;

        while (stringTokenizer.hasMoreTokens()) {
            String nrRelNode = stringTokenizer.nextToken();
            System.out.println("[INFO] -----------------------------------------");
            System.out.println("GET DATA FROM NODE NR : " + nrRelNode);

            Metadates relationServerMetadates = ReadServerLocationDatesHelper.getModeMetadates(nrRelNode);
            arrayListFuture[i++] = service.submit(new TransportClient(relationServerMetadates, jsonInputStream));
        }

        return arrayListFuture;
    }

    private ArrayList<Employee> getEmployeesFromFuture(Future<ArrayList<Employee>> [] arrayListFuture, ArrayList<Employee> employees) {

        for (Future<ArrayList<Employee>> listFuture: arrayListFuture) {
            try {
                employees.addAll(listFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return employees;
    }

}
